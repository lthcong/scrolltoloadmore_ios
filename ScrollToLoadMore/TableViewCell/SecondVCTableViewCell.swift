//
//  SecondVCTableViewCell.swift
//  ScrollToLoadMore
//
//  Created by Cong La on 5/27/19.
//  Copyright © 2019 Cong La. All rights reserved.
//

import UIKit

class SecondVCTableViewCell: UITableViewCell {

    @IBOutlet weak var vItemView: UIView!
    @IBOutlet weak var lbItemNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vItemView.layer.cornerRadius = CGFloat(10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
