//
//  FirstViewController.swift
//  ScrollToLoadMore
//
//  Created by Cong La on 5/26/19.
//  Copyright © 2019 Cong La. All rights reserved.
//

import UIKit
import UserNotifications

class FirstViewController: UIViewController {
    
    @IBOutlet weak var tfEnteredNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let TestingNotification = UNMutableNotificationContent()
        TestingNotification.title = "Testing Notification"
        TestingNotification.body = "First Notification"
        TestingNotification.sound = UNNotificationSound.default
        
        let NotificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let NotificationRequest = UNNotificationRequest(identifier: "TestingNotificationIdentifier", content: TestingNotification, trigger: NotificationTrigger)
        
        UNUserNotificationCenter.current().add(NotificationRequest, withCompletionHandler: nil)
        
        print("show notification")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func submitEnteredNumber(_ sender: Any) {
        InfoCollector.EnteredNumber = (Int(tfEnteredNumber.text!.description)! + 5 ) * 1000        
        performSegue(withIdentifier: "ShowSecondViewController", sender: self)
    }
    

}
