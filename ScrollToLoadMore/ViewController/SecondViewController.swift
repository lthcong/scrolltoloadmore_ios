//
//  ViewController.swift
//  ScrollToLoadMore
//
//  Created by Cong La on 5/26/19.
//  Copyright © 2019 Cong La. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbvItem: UITableView!
    
    var ItemArray:[String] = [String]()
    let NumberOfItem = InfoCollector.EnteredNumber
    var NumberOfShowedItem = 20
    let NumberOfIncreasement = 20
    
    func getItemArray() -> Void {
        for i in 0 ..< NumberOfShowedItem {
            ItemArray.append(String(i))
        }
        
        tbvItem.reloadData()
    }
    
    func loadMoreItem() -> Void {
        if (NumberOfShowedItem < NumberOfItem) {
            let LastItem = (ItemArray.last?.description)!
            for i in 1 ... NumberOfIncreasement {
                ItemArray.append(String(Int(LastItem)! + i))
            }
        
            tbvItem.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ItemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecondVCTableViewCellIdentifier", for: indexPath) as! SecondVCTableViewCell
        
        cell.lbItemNumber.text = "Item #" + ItemArray[indexPath.row]
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if (offsetY > (contentHeight - scrollView.frame.height * 5)) {
            loadMoreItem()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getItemArray()
    }
    
    
    @IBAction func navigateBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    

}

